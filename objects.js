
let objTelefonos = {
    tel_1: "123456",
    tel_2: "7890"
}

let objPersona_1 = {
    nombre: "Andrés",
    apellido: "Quintero",
    email: "andres@gmail.com",
    telefono: objTelefonos,
    edad: 20
}

let objPersona_2 = {
    nombre: "Ana maria",
    apellido: "Hernan",
    email: "anamaria@gmail.com",
    telefono: objTelefonos,
    edad: 20
}

let objPersona_3 = {
    nombre: "Juan",
    apellido: "Navajas",
    email: "juan@gmail.com",
    telefono: objTelefonos,
    edad: 22
}

let objPersona_4 = {
    nombre: "Pedro",
    apellido: "Alimaña",
    email: "pedro@gmail.com",
    telefono: objTelefonos,
    edad: 20
}

console.log(objPersona_1['nombre']);

let personas = [ objPersona_1, objPersona_2, objPersona_3, objPersona_4 ];

console.log(personas);

personas.forEach(objPersona => {
    console.log(objPersona.nombre);
});

/*Acceder a los atributos de un objeto
console.log( objPersona.nombre );
console.log( objPersona.apellido );
console.log( objPersona.email );
*/

/*
//EJERCICIO:
//Escribir una función que reciba como parámetro un objeto,
//Validar que ningún atributo sea nulo y añadirlo al arreglo 'personas',
//si algún atributo es nulo no se añadirá el objeto al arreglo
//Estructura del objeto: 
let objPersona_4 = {
    nombre: "Pedro",
    apellido: "Alimaña",
    email: "pedro@gmail.com",
    telefono: objTelefonos,
    edad: 20
}*/

function agregarPersona(objPersona){
    if( objPersona.nombre != null && objPersona.apellido != null 
        && objPersona.email != null && objPersona.telefono != null 
        && objPersona.edad != null ){
            //añadir al arreglo
            personas.push(objPersona);
        }
}

let objPersona = {
    nombre: "Juliana",
    apellido: "Fernandez",
    email: "juliana@gmail.com",
    telefono: objTelefonos,
    edad: 25
}

agregarPersona(objPersona);

console.log( personas );


/********* 
*Ejercicio:
*Escriba una función que reciba como parámetro:
*nombre, apellido, email, telefono y edad
*retorne un objeto con estos datos y posteriormente 
*añádalo al arreglo 'personas'
*
********/

function crearObjeto(nombre, apellido, email, telefono, edad){
    //Construir el objeto
    let objPersona = {
        nombre,
        apellido,
        email,
        telefono,
        edad
    }
    //retornar el objeto
    return objPersona;
}

let obj = crearObjeto("Andrés", "Peñaranda", "andres@gmail.com", "1234567", 25);


//añadir el objeto al arreglo
personas.push(obj);

console.log( personas );