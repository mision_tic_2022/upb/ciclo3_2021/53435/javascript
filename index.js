
//VARIABLES
//let tiene alcance local
let numero_1 = 1;
//var tiene un alcance global
var cadena = "hola mundo";
//constante
const numero_2 = 2;

numero_1 = 5;
cadena = "Hola mundo";

//MOSTRAR MENSAJES EN CONSOLA
console.log(cadena);
console.error("Esto es un error");
console.table({ nombre: "Andrés" });


//----ESTRUCTURAS DE DECISIÓN------
console.log("--------------ESTRUCTURAS DE DECISIÓN----------------");

if (numero_1 > numero_2) {
    console.log("Número 1 es mayor");
} else {
    console.log("Número 2 es mayor")
}

let numero_3 = 10;
let numero_4 = 5;

//AND
if (numero_1 > numero_2 && numero_3 > numero_4) {
    console.log("Cumple AND");
} else {
    console.log("No cumple");
}

//OR
if (numero_1 > numero_2 || numero_3 > numero_4) {
    console.log("Cumple OR");
} else {
    console.log("No cumple");
}


//-----CICLOS-----
console.log("--------------CICLOS----------------");

//FOR
for (let i = 0; i < 10; i++) {
    console.log(i);
}

console.log("-------WHILE-----")
let contador = 0;
while (contador < 5) {
    console.log(contador);
    ++contador;
}

console.log("----------NUMEROS PARES-----");
//IMPRIMIR TODOS LOS NÚMEROS PARES DE 1 AL 100
/*for(let i = 1; i <= 100; i++){
    if(i%2 == 0){
        console.log(i);
    }
}*/
/*
let contador_2 = 2 ;
while (contador_2 <=100){
    console.log(contador_2);
    contador_2 =contador_2 + 2;
}*/



console.log("--------------ARREGLOS---------");

let numeros = [];
console.log("Números-> ", numeros);
console.log("Tamaño del arreglo-> ", numeros.length);

//Añadir elementos en pila
numeros.push(10);
numeros.push(30);
numeros.push(20);
console.log("Tamaño del arreglo-> ", numeros.length);

console.log("Números -> ", numeros);

//EJERCICIO: Eliminar el número 20 del arreglo
nuevoArreglo = []
for (let i = 0; i < numeros.length; i++) {
    if (numeros[i] != 20) {
        nuevoArreglo.push(numeros[i])
    }
}
console.log("Nuevo arreglo->  ", nuevoArreglo)


console.log( numeros[ numeros.length-1 ] );


/*
//Quitar el último elemento
numeros.pop();
console.log("Números -> ", numeros);


let frutas = ['Manzana', 'Pera', 'Piña', 'Melocotón'];
console.log(frutas);
*/
