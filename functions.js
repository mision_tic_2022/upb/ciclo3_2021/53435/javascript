
//--------FUNCIONES-------

function sumar(num_1, num_2) {
    let suma = num_1 + num_2;
    return suma;
}

//Llamar función
let suma_1 = sumar(1, 5);
let suma_2 = sumar(2, 8);
let suma_3 = sumar(10, 8);

console.log("Suma_1: ", suma_1, "\nSuma_2: ", suma_2, "\nSuma_3: ", suma_3);


console.log("---------FUNCIONES FLECHA-------")

const multiplicar = (num_1, num_2) => {
    return num_1 * num_2;
}

//console.log( multiplicar(2, 10) );

function funcion_orden_superior(operacion) {
    let resultado = operacion(2, 10) + 20;
    console.log("Resultado en F. orden superior-> ", resultado);
}


funcion_orden_superior(multiplicar);

console.log("---------FABRICA DE OPERACIONES-------")

function crear_operacion(operador) {
    let operacion;
    if (operador == '+') {
        operacion = (num_1, num_2) => {
            return num_1 + num_2;
        }
    } else if (operador == '-') {
        operacion = (num_1, num_2) => {
            return num_1 - num_2;
        }
    } else if (operador == '/') {
        operacion = (num_1, num_2) => {
            return num_1 / num_2;
        }
    } else if (operador == '*') {
        operacion = (num_1, num_2) => {
            return num_1 * num_2;
        }
    }

    return operacion;
}

let operacion = crear_operacion('*');
let resultado = operacion(2, 5);
console.log("Multiplicacion-> ", resultado);

operacion = crear_operacion('-');
resultado = operacion(20, 5);
console.log("Resta-> ", resultado);

console.log("-------------ARREGLO--------");

let numeros = [];
for (let i = 0; i < 200; i++) {
    numeros.push(i);
}

numeros.forEach(element => {
    console.log(element);
});

//Transformar los datos del arreglo
numeros.map( element => element = element * 2 );

//Búscar elemento
let filtro_numeros = numeros.filter( element => (element > 150) );

console.log(filtro_numeros);


let frutas = ["Manzana", "Pera", "Sandia", "Piña", "Durazno"];

//Obtener todas las frutas que comienzan con la letra P
//variable_string.charAt(pos) -> obtener la letra de una posicion en específico

let iniciaConP = frutas.filter(
    element => (element.charAt(0) == "P")
)

console.log(iniciaConP);