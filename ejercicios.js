
/************+
 * -Obtener el resultado de la potencia de un número:
 *      Math.pow(num, potencia)
 * -Obtener raíz cuadrada:
 *      Math.sqrt(numero)
 * -Obtener seno de un número:
 *      Math.sin(numero)
 * -Obtener números aleatorios entre 0 y 1:
 *      Math.random(numero)
 */

/********************
 * EJERCICIOS:
 * 1) Escriba una función que reciba como parámetro dos números, retorne la sumatoria 
 * de los valores al cuadrado.
 * 2) Escriba una función que reciba como parámetro el nombre de un usuario, 
 * imprima por consola: 'Hola nombre_usuario'
 * 3) Escriba una función que reciba como parámetro un número, retorne true si el número es 
 * par, false de lo contrario
 * 4) Escriba una función que reciba dos parámetros; un arreglo y el valor a eliminar del arreglo,
 * retorne el nuevo arreglo.
 *******************/

//Cindy
function elevarSumatoria(a, b) {
    resultado = (a + b) ^ 2
    return resultado
}
function saludar(nombre) {
    console.log("Hola", nombre)
}

//Christopher
function metodo3(nu_1) {
    if (nu_1 % 2 == 0) {
        return true;
    }
    return false;
}


function metodo4(arr, nu_1) {
    let valor = 0;
    let salida = [];
    while (valor <= arr.length) {
        if (arr[valor] !== nu_1) {
            salida.push(arr[valor]);
        }

        valor++;
    }
    return salida;

}